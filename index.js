const express = require("express");
const app = express();
const MongoClient = require("mongodb").MongoClient;
const { uri, db } = require("./db/config");
//const db = require("./db/config"); //подключаем модуль конфиг
const bodyParser = require("body-parser");
app.use("/assets", express.static("public")); //подключаем папку для статических файлов

app.use(bodyParser.urlencoded({ extended: false }));
app.set("views", "./app/views/");

app.use(bodyParser.json());
// set the view engine to ejs
app.set("view engine", "ejs"); // use res.render to load up an ejs view file
//------------------------------файлы-подключения------------------------------------------//
// home page
app.get("/", function(req, res) {
  res.render("pages/index");
});

// new list page
app.get("/list", async function(req, res) {
  res.render("pages/list");
  console.log("Hello hesh-list");
});

// new note page
app.get("/note", async function(req, res) {
  const Name = "Anastasiia";
  res.render("pages/note", { Name });
  console.log("Hello hesh-note");
});

app.post("/note", async function(req, res) {
  console.log(req.body);
  const note = {
    noteText: req.body.noteText
  };
  await db.addNote(note);
  res.end();
});

app.get("/contacts", async function(req, res) {
  res.render("pages/contacts");
});

const testConnection = async () => {
  const client = new MongoClient(uri, { useNewUrlParser: true });
  await client.connect();
  console.log("connected");
  client.close();
};
const port = 3000;

app.listen(port, () => {
  console.log("We are live on " + port);
  console.log("Connected to " + db.url);
});

module.exports = {
  testConnection,
  app
};
