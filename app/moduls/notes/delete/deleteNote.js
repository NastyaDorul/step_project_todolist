const MongoClient = require("mongodb").MongoClient;
const { uri, db } = require("./config");

const testConnection = async () => {
  const client = new MongoClient(uri, { useNewUrlParser: true });
  await client.connect();
  console.log("connected");
  client.close();
};

const getNote = async () => {
  const client = new MongoClient(uri, { useNewUrlParser: true });
  await client.connect();
  const noteCollection = await client.db(db).collection("Notices");
  const data = await noteCollection.find({}).toArray();
  client.close();
  console.log(data);
  return data;
};

const deleteNote = async note => {
  const client = new MongoClient(uri, { useNewUrlParser: true });
  await client.connect();
  const noteCollection = await client.db(db).collection("Notices");
  await noteCollection.deleteOne(note);
  console.log("deleted one note");
  client.close();
};

module.exports = {
  testConnection,
  getNote,
  deleteNote
};
