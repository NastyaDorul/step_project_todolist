const MongoClient = require("mongodb").MongoClient;
const { uri, db } = require("./config");

const testConnection = async () => {
  const client = new MongoClient(uri, { useNewUrlParser: true });
  await client.connect();
  console.log("connected");
  client.close();
};

const addNote = async note => {
  const client = new MongoClient(uri, { useNewUrlParser: true });
  await client.connect();
  const noteCollection = await client.db(db).collection("Notices");
  await noteCollection.insertOne(note);
  console.log("1 note inserted");
  client.close();
};

module.exports = {
  testConnection,
  addNote,
  getNote
};
